export const blue = {

  '--primary' : '#ffe14a',  // buttons
  '--primary-variant': '#181818', // visited Sessions button

  '--secondary': '#82dc27', // frag.jetzt
  '--secondary-variant': '#82dc27',

  '--background': '#202020',  //background
  '--surface': '#181818', // borders
  '--dialog': '#181818',  // impressum background
  '--alt-surface': '#181818',
  '--alt-dialog': '#181818',

  '--on-primary': '#000000',  // writing in buttons
  '--on-secondary': '#141414',  // writing in button save in new session
  '--on-background': '#FFFFFF', // arrow in color Theme
  '--on-surface': '#FFFFFF',  // writing color

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': 'blue',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': 'grey',
  '--grey-light': 'lightgrey',
  '--black': 'black',
  '--moderator': '#37474f'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'THM Green - Dark',
      'de': 'THM Grün - Dunkel'
    },
    'description': {
      'en': 'Dark Theme with color plate derived from THM Green',
      'de': 'Dunkles Farbschema mit Farbpalette der THM Farben'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'secondary'

};
